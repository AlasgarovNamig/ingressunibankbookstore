package com.bookstore.bookstore.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.util.ReflectionTestUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
//@MockitoSettings(strictness = Strictness.LENIENT)
class SendMailServiceImplTest {
    @Mock
    private JavaMailSender javaMailSender;

    @InjectMocks
    private SendMailServiceImpl sendMailService;

    @Mock
    private MimeMessage message;


    @Test
    void send() throws MessagingException {

        //Arrange
        doNothing().when(javaMailSender).send(any(MimeMessage.class));
        when(javaMailSender.createMimeMessage()).thenReturn(message);
        ReflectionTestUtils.setField(sendMailService, "hostMail", "host");
//        ReflectionTestUtils.setField(sendMailService, "resourceFile", new ClassPathResource("path"));

        //Act
        sendMailService.send("sendTo", "Subject", "htmlBody");

        //Assert
        verify(javaMailSender, times(1)).send(any(MimeMessage.class));
    }
}