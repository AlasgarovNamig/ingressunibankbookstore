package com.bookstore.bookstore.service.impl;

import com.bookstore.bookstore.dto.request.BookDto;
import com.bookstore.bookstore.dto.response.BookResponseDto;
import com.bookstore.bookstore.dto.response.UserResponseDto;
import com.bookstore.bookstore.model.Authority;
import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.model.User;
import com.bookstore.bookstore.model.enums.UserType;
import com.bookstore.bookstore.repository.BookRepository;
import com.bookstore.bookstore.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {

    private static final Long DUMMY_USER_DTO_ID = 1l;
    private static final String DUMMY_USER_DTO_FULL_NAME = "NAMIG ALASGAROV";

    private static final String DUMMY_BOOK_DTO_NAME = "DUMMY BOOK";
    private static final String DUMMY_BOOK_DTO_TITLE = "DUMMY TITLE";
    private static final String DUMMY_BOOK_DTO_AUTHOR = "DUMMY AUTHOR";
    private static final BigDecimal DUMMY_BOOK_DTO_COAST = new BigDecimal(3.5);


    private static final Long DUMMY_USER_ID = 1l;
    private static final String DUMMY_USER_FULL_NAME = "NAMIG ALASGAROV";
    private static final String DUMMY_USER_EMAIL = "sr.alasgarov@gmail.com";
    private static final String DUMMY_USER_PASSWORD = "$2a$10$Ud/qyIJkWqkH.koMaYslYeEIWbES.W0VH.Uk4QBVrBpBouDn/pC8S";
    private static final UserType DUMMY_USER_TYPE = UserType.USER;


    private static final Long DUMMY_BOOK_ID = 1l;
    private static final String DUMMY_BOOK_NAME = "DUMMY BOOK";
    private static final String DUMMY_BOOK_TITLE = "DUMMY TITLE";
    private static final String DUMMY_BOOK_AUTHOR = "DUMMY AUTHOR";
    private static final BigDecimal DUMMY_BOOK_COAST = new BigDecimal(3.5);


    private static final PageRequest DUMMY_PAGE = PageRequest.of(0, 20);

    @Spy
    private ModelMapper modelMapper;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private Authentication authentication;

    @InjectMocks
    private BookServiceImpl bookService;

    private List<Book> dummyBookList;
    private List<BookResponseDto> dummyBookDtoList;
    private BookResponseDto bookResponseDto;
    private UserResponseDto userResponseDto;
    private Book book;
    private User user;
    private BookDto bookDto;
    private Set<Authority> authorities;


    @BeforeEach
    void setUp() {
        userResponseDto = UserResponseDto.builder()
                .id(DUMMY_USER_DTO_ID)
                .fullName(DUMMY_USER_DTO_FULL_NAME)
                .build();

        bookResponseDto = BookResponseDto.builder()
                .name(DUMMY_BOOK_DTO_NAME)
                .title(DUMMY_BOOK_DTO_TITLE)
                .author(DUMMY_BOOK_DTO_AUTHOR)
                .coast(DUMMY_BOOK_DTO_COAST)
                .publisher_id(userResponseDto)
                .build();

        dummyBookDtoList = Arrays.asList(bookResponseDto);
        authorities = Set.of(Authority.builder().name("USER").build(), Authority.builder().name("PUBLISHER").build());

        user = User.builder()
                .id(DUMMY_USER_ID)
                .fullName(DUMMY_USER_FULL_NAME)
                .email(DUMMY_USER_EMAIL)
                .password(DUMMY_USER_PASSWORD)
                .userType(DUMMY_USER_TYPE)
                .authorities(authorities)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();

        book = Book.builder()
                .id(DUMMY_BOOK_ID)
                .name(DUMMY_BOOK_NAME)
                .title(DUMMY_BOOK_TITLE)
                .author(DUMMY_BOOK_AUTHOR)
                .coast(DUMMY_BOOK_COAST)
                .publisher_id(user)
                .build();

        dummyBookList = Arrays.asList(book);

        bookDto = BookDto.builder()
                .name(DUMMY_BOOK_NAME)
                .title(DUMMY_BOOK_TITLE)
                .author(DUMMY_BOOK_AUTHOR)
                .coast(DUMMY_BOOK_COAST)
                .build();

    }

    @Test
    void getAll() {
        //Arrange
        when(bookRepository.findAll()).thenReturn(dummyBookList);

        // Act and Assert
        assertThat(bookService.getAll()).isEqualTo(dummyBookDtoList);
    }

    @Test
    void searchName() {
        // Arrange
        when(bookRepository
                .findByNameLike(any(String.class), any(Pageable.class)))
                .thenReturn(Page.empty());

        // Act
        bookService.searchName(new String(), Pageable.unpaged());

        // Assert
        verify(bookRepository, times(1))
                .findByNameLike(any(String.class), any(Pageable.class));
    }

    @Test
    void bookDetail() {
        // Arrange
        when(bookRepository.findById(1L)).thenReturn(Optional.of(book));

        //Act and Assert
        assertThat(bookService.bookDetail(1L)).isEqualTo(bookResponseDto);
        Mockito.verify(bookRepository, Mockito.times(1)).findById(1L);

    }

    @Test
    void publisherBooks() {
        // Arrange
        when(bookRepository.findByPublisher_id(1L)).thenReturn(dummyBookList);

        //Act and Assert
        assertThat(bookService.publisherBooks(1L)).isEqualTo(dummyBookDtoList);
        Mockito.verify(bookRepository, Mockito.times(1)).findByPublisher_id(1L);
    }

    @Test
    void bookCreate() {
        // Arrange
        when(userRepository.findByEmail(null)).thenReturn(Optional.of(user));

        //Act
        bookService.bookCreate(bookDto, authentication);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail(null);
        Mockito.verify(bookRepository, Mockito.times(1)).save(any(Book.class));
    }

    @Test
    void bookUpdate() {
        // Arrange
        when(userRepository.findByEmail(null)).thenReturn(Optional.of(user));
        when(bookRepository.findById(1L)).thenReturn(Optional.of(book));

        //Act
        bookService.bookUpdate(bookDto, authentication, 1L);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail(null);
        Mockito.verify(bookRepository, Mockito.times(1)).findById(1L);
        Mockito.verify(bookRepository, Mockito.times(1)).save(any(Book.class));
    }

    @Test
    void bookUpdateForBadRequest() {
        // Arrange
        User u = User.builder().id(5l).build();
        when(userRepository.findByEmail(null)).thenReturn(Optional.of(u));

        when(bookRepository.findById(1L)).thenReturn(Optional.of(book));

        //Act
        bookService.bookUpdate(bookDto, authentication, 1L);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail(null);
        Mockito.verify(bookRepository, Mockito.times(1)).findById(1L);

    }
}