package com.bookstore.bookstore.service.impl;

import com.bookstore.bookstore.dto.request.BookDto;
import com.bookstore.bookstore.dto.request.SignUpDto;
import com.bookstore.bookstore.dto.request.UserInfoDto;
import com.bookstore.bookstore.dto.response.BookResponseDto;
import com.bookstore.bookstore.dto.response.UserResponseDto;
import com.bookstore.bookstore.exception.EmailAlreadyUsedException;
import com.bookstore.bookstore.model.Authority;
import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.model.User;
import com.bookstore.bookstore.model.enums.UserType;
import com.bookstore.bookstore.repository.AuthorityRepository;
import com.bookstore.bookstore.repository.UserRepository;
import com.bookstore.bookstore.security.SecurityProperties;
import com.bookstore.bookstore.security.service.ClaimSetProvider;
import com.bookstore.bookstore.security.service.JwtService;
import com.bookstore.bookstore.service.UserRegistrationService;
import freemarker.template.TemplateException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplTest {
    private static final Long DUMMY_USER_ID = 1l;
    private static final String DUMMY_USER_FULL_NAME = "NAMIG ALASGAROV";
    private static final String DUMMY_USER_EMAIL = "sr.alasgarov@gmail.com";
    private static final String DUMMY_USER_PASSWORD = "$2a$10$Ud/qyIJkWqkH.koMaYslYeEIWbES.W0VH.Uk4QBVrBpBouDn/pC8S";
    private static final UserType DUMMY_USER_TYPE = UserType.USER;

    @Mock
    private UserRepository userRepository;

    @Mock
    private AuthorityRepository authorityRepository;

    @Mock
    private UserRegistrationService userRegistrationService;

    @Spy
    private ModelMapper modelMapper;


    @InjectMocks
    private UserServiceImpl userService;

    private User user;
    private User userSecond;
    private SignUpDto signUpDto;
    private Set<Authority> authorities;

    @BeforeEach
    void setUp() {

        authorities = Set.of(Authority.builder().name("USER").build(), Authority.builder().name("PUBLISHER").build());

        user = User.builder()
                .id(DUMMY_USER_ID)
                .fullName(DUMMY_USER_FULL_NAME)
                .email(DUMMY_USER_EMAIL)
                .password(DUMMY_USER_PASSWORD)
                .userType(DUMMY_USER_TYPE)
                .authorities(authorities)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();

        userSecond = User.builder()
                .id(DUMMY_USER_ID)
                .fullName(DUMMY_USER_FULL_NAME)
                .email(DUMMY_USER_EMAIL)
                .password(DUMMY_USER_PASSWORD)
                .userType(UserType.PUBLISHER)
                .authorities(authorities)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();

         signUpDto = SignUpDto.builder()
                .fullName(DUMMY_USER_FULL_NAME)
                .email(DUMMY_USER_EMAIL)
                .password(DUMMY_USER_PASSWORD)
                .userType(DUMMY_USER_TYPE).build();

    }

    @Test
    void loadUserByUsername() {
        // Arrange
        when(userRepository.findByEmail(null)).thenReturn(Optional.of(user));

        //Act and Assert
        assertThat(userService.loadUserByUsername(any(String.class))).isEqualTo(user);
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail(null);

    }

    @Test
    void givenGetUserIdExpectNotFountException() {
        assertThatThrownBy(() -> userService.loadUserByUsername(null))
                .isInstanceOf(UsernameNotFoundException.class).hasMessage("User null was not found in the database");
    }


    @Test
    void signUp() throws IOException, TemplateException, MessagingException {

        // Arrange
        doNothing().when(userRegistrationService).sendActivationEmail(any(UserInfoDto.class));
        when(authorityRepository.findByName("USER")).thenReturn(Optional.of(Authority.builder().name("USER").build()));
        when(authorityRepository.findByName("PUBLISHER")).thenReturn(Optional.of(Authority.builder().name("PUBLISHER").build()));
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());

        //Act
        userService.signUp(signUpDto);
        signUpDto.setUserType(UserType.PUBLISHER);
        userService.signUp(signUpDto);

        //Assert
        Mockito.verify(userRepository, Mockito.times(2)).findByEmailAndAccountNonExpiredAndAccountNonLockedAndCredentialsNonExpiredAndEnabled(DUMMY_USER_EMAIL, true, true, true, true);
        Mockito.verify(userRepository, Mockito.times(2)).save(any(User.class));
    }

    @Test
    void whenUserExistExpectEmailAlreadyUsedException() {
        //Arrange
        when(userRepository.findByEmailAndAccountNonExpiredAndAccountNonLockedAndCredentialsNonExpiredAndEnabled(DUMMY_USER_EMAIL,true,true,true,true))
                .thenReturn(Optional.of(user));

        //Act and Assert
        assertThatThrownBy(() -> userService.signUp(signUpDto))
                .isInstanceOf(EmailAlreadyUsedException.class).hasMessage("Email 'sr.alasgarov@gmail.com'  already registered,try different email");
    }

    @Test
    void activate() {
        // Arrange
        when(userRepository.findByEmail("sr1.alasgarov@gmail.com")).thenReturn(Optional.of(user));
//        doNothing().when(userRepository).save(any(User.class));

        userService.activate("eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiJzcjEuYWxhc2dhcm92QGdtYWlsLmNvbSIsImlhdCI6MTY1MDY5NzIzNywiZXhwIjoxNjUxMzAyMDM3LCJydWxlIjpbIlVTRVIiXX0.Pczj6hLsD13yf4RffuT-NR7RWl85lwgRl39pfebxcwFALsmV0dt94cN2N3J4k0es3J_kmdPz4r2kebm9oRZ5dg");
//        userService.activate("eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiJzcjEuYWxhc2dhcm92QGdtYWlsLmNvbSIsImlhdCI6MTY1MDY5NzIzNywiZXhwIjoxNjUwNzA3MjM3LCJydWxlIjpbIlVTRVIiXX0.A5NrGWjD-UBO29xxPFG1Y1EXJZNCyJLV1rXQjUhdplgbfSYlCMIf9m7SDT3NvPNRlFygMAfAwMInZLocnB-Jlw");


        Mockito.verify(userRepository, Mockito.times(1)).findByEmail("sr1.alasgarov@gmail.com");
        Mockito.verify(userRepository, Mockito.times(1)).save(any(User.class));

    }
//    @Test
//    void activateTokenParseErrororExpired() {
//        // Arrange
////        when(userRepository.findByEmail("sr1.alasgarov@gmail.com")).thenReturn(Optional.of(user));
////        doNothing().when(userRepository).save(any(User.class));
//
////        userService.activate("eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiJzcjEuYWxhc2dhcm92QGdtYWlsLmNvbSIsImlhdCI6MTY1MDY5NzIzNywiZXhwIjoxNjUwNzA3MjM3LCJydWxlIjpbIlVTRVIiXX0.A5NrGWjD-UBO29xxPFG1Y1EXJZNCyJLV1rXQjUhdplgbfSYlCMIf9m7SDT3NvPNRlFygMAfAwMInZLocnB-Jlw");
//
////        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
////        Date date = new Date(System.currentTimeMillis());
//
////        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
////        Date date = new Date(System.currentTimeMillis());
////        System.out.println(formatter.format(date));
//        assertThatThrownBy(() -> userService.activate("eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiJzcjEuYWxhc2dhcm92QGdtYWlsLmNvbSIsImlhdCI6MTY1MDY5NzIzNywiZXhwIjoxNjUwNzA3MjM3LCJydWxlIjpbIlVTRVIiXX0.A5NrGWjD-UBO29xxPFG1Y1EXJZNCyJLV1rXQjUhdplgbfSYlCMIf9m7SDT3NvPNRlFygMAfAwMInZLocnB-Jlw"))
//                .isInstanceOf(ExpiredJwtException.class);
////                .hasMessage("JWT expired at 2022-04-23T09:47:17Z. Current time: "+formatter.format(date)+", a difference of 32043029 milliseconds.  Allowed clock skew: 0 milliseconds.");
////        Mockito.verify(userRepository, Mockito.times(1)).findByEmail("sr1.alasgarov@gmail.com");
////        Mockito.verify(userRepository, Mockito.times(1)).save(any(User.class));
//
//    }
}