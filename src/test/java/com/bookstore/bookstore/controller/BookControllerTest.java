package com.bookstore.bookstore.controller;

import com.bookstore.bookstore.repository.BookRepository;
import com.bookstore.bookstore.security.SecurityConfiguration;
import com.bookstore.bookstore.service.BookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = SecurityConfiguration.class, loader = AnnotationConfigContextLoader.class)
class BookControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired(required = false)
    private BookRepository bookRepository;

    @MockBean
    private BookService bookService;


    @Test
    void list() {
    }

    @Test
    void searchName() {
    }

    @Test
    void bookDetail() {
    }

    @Test
    void publisherBook() {
    }

    @Test
    void createBook() {
    }

    @Test
    void updateBook() {
    }
}