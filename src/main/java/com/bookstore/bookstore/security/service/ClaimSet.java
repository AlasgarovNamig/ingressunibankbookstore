package com.bookstore.bookstore.security.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClaimSet {

    private String key;
    private Set<String> claims;
}
