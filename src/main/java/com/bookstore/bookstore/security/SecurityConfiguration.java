package com.bookstore.bookstore.security;


import com.bookstore.bookstore.security.service.JwtService;
import com.bookstore.bookstore.security.service.TokenAuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;


@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {

    private final SecurityProperties securityProperties;
    private  final JwtService jwtService;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .cors().configurationSource(corsConfigurationSource());//
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests().antMatchers("/auth/sign-in",
                "/auth/sign-up",
                "/auth/activate"
        ).permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/books").authenticated();
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/books/{search_name}").authenticated();
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/books/detail/{id}").authenticated();
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/books/{publiser_id}/books").hasAuthority("USER");
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/books").hasAuthority("PUBLISHER");
        http.authorizeRequests().antMatchers(HttpMethod.PUT, "/books/{id}").hasAuthority("PUBLISHER");

        http.apply(new JwtAuthFilterConfigurerAdapter(List.of(new TokenAuthService(jwtService))));
        super.configure(http);
    }


    protected CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = securityProperties.getCors();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}
