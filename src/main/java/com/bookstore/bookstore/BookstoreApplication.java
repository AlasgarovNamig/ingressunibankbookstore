package com.bookstore.bookstore;

import com.bookstore.bookstore.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;

@SpringBootApplication
public class BookstoreApplication  {


//	@Autowired
//	BookRepository bookRepository;
//	@Autowired
//	ModelMapper modelMapper;
	public static void main(String[] args) {
		SpringApplication.run(BookstoreApplication.class, args);
	}


//	@Override
//	@Transactional
//	public void run(String... args) throws Exception {
////		System.out.println(bookRepository.findById(1l).map(book -> modelMapper.map(book, BookResponseDtos.class)));
////		System.out.println(bookRepository.findByNameLike("%al%"));
////		System.out.println(bookRepository.findByPublisher_id(1l));
//	}
}
