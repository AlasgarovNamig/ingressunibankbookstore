package com.bookstore.bookstore.controller;


import com.bookstore.bookstore.dto.response.AccessTokenDto;
import com.bookstore.bookstore.dto.request.SignInDto;
import com.bookstore.bookstore.dto.request.SignUpDto;
import com.bookstore.bookstore.dto.request.TokenPayloadDto;
import com.bookstore.bookstore.security.service.JwtService;
import com.bookstore.bookstore.service.UserService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

//import javax.validation.Valid;
import java.time.Duration;
import java.util.Date;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_WEEK = Duration.ofDays(7);

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;
    private final UserService userService;


    @PostMapping("/sign-in")
    public ResponseEntity<AccessTokenDto> authorize(@RequestBody SignInDto signInDto) {
        log.trace("Authenticating user {}", signInDto.getEmail());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(signInDto.getEmail(),
                signInDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Duration duration = getDuration(signInDto.getRememberMe());
        String jwt = jwtService.issueToken(authentication, duration);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new AccessTokenDto(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Void> signUp(@RequestBody SignUpDto signUpDto) {
        log.trace("Sign up request with email {}", signUpDto.getEmail());
        userService.signUp(signUpDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/activate")
    public ResponseEntity<Void> activate(@RequestParam("alma") String alma) {
        return userService.activate(alma);
    }



    private Duration getDuration(Boolean rememberMe) {
        return ((rememberMe != null) && rememberMe) ? ONE_WEEK : ONE_DAY;
    }


}
