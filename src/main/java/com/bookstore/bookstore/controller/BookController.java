package com.bookstore.bookstore.controller;

import com.bookstore.bookstore.dto.request.BookDto;
import com.bookstore.bookstore.dto.response.BookResponseDto;
import com.bookstore.bookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @GetMapping()
    public ResponseEntity<List<BookResponseDto>> list() {
        return ResponseEntity.ok(bookService.getAll());
    }

    @GetMapping("/{search_name}")
    public ResponseEntity<Page<BookResponseDto>> searchName(@PathVariable String search_name, Pageable pageable) {
        return ResponseEntity.ok(bookService.searchName(search_name, pageable));
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<BookResponseDto> bookDetail(@PathVariable Long id) {
        return ResponseEntity.ok(bookService.bookDetail(id));
    }


    @GetMapping("/{publiser_id}/books")
    public ResponseEntity<List<BookResponseDto>> publisherBook(@PathVariable Long publiser_id) {
        return ResponseEntity.ok(bookService.publisherBooks(publiser_id));
    }

    @PostMapping()
    public ResponseEntity<Void> createBook(@RequestBody BookDto bookCreateDto, Authentication authentication) {
        bookService.bookCreate(bookCreateDto, authentication);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateBook(@RequestBody BookDto bookCreateDto, Authentication authentication, @PathVariable Long id) {
        return bookService.bookUpdate(bookCreateDto, authentication, id);
    }

}
