package com.bookstore.bookstore.exception;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.bookstore.bookstore.constants.HttpResponseConstants.*;
//        az.ssms.usermanagmentms.constants.HttpResponseConstants.*;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends DefaultErrorAttributes {
    @ExceptionHandler({BadCredentialsException.class})
    public final ResponseEntity<Map<String, Object>> handle(BadCredentialsException ex, WebRequest request) {
        log.trace("Service unavailable {}", "username and password are wrong");
        return ofType(request, HttpStatus.BAD_REQUEST, "username and password are wrong");
    }

    @ExceptionHandler({ExpiredJwtException.class})
    public final ResponseEntity<Map<String, Object>> handle(ExpiredJwtException ex, WebRequest request) {
        log.trace("Service unavailable {}", "token expired !!!  Retry register please or login please");
        return ofType(request, HttpStatus.BAD_REQUEST, "token expired !!!  Retry register please or login please");
    }

    @ExceptionHandler({EmailAlreadyUsedException.class})
    public final ResponseEntity<Map<String, Object>> handle(EmailAlreadyUsedException ex, WebRequest request) {
        log.trace("Service unavailable {}", ex.getMessage());
        return ofType(request, HttpStatus.BAD_REQUEST, ex.getMessage());
    }

//    @ExceptionHandler({BookNotFoundException.class})
//    public final ResponseEntity<Map<String, Object>> handle(BookNotFoundException ex, WebRequest request) {
//        log.trace("Service unavailable {}", ex.getMessage());
//        return ofType(request, HttpStatus.BAD_REQUEST, ex.getMessage());
//    }
    @ExceptionHandler({BookNotFoundException.class})
    public final ResponseEntity<Map<String, Object>> handle(BookNotFoundException ex, WebRequest request) {
        log.trace("Service unavailable {}", ex.getMessage());
        return ofType(request, HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message) {
        return ofType(request, status, message, Collections.EMPTY_LIST);
    }

    private ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message,
                                                       List validationErrors) {
        Map<String, Object> attributes = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        attributes.put(STATUS, status.value());
        attributes.put(ERROR, status.getReasonPhrase());
        attributes.put(MESSAGE, message);
        attributes.put(ERRORS, validationErrors);
        attributes.put(PATH, ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(attributes, status);
    }
}
