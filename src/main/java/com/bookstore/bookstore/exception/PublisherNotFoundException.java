package com.bookstore.bookstore.exception;

public class PublisherNotFoundException  extends  RuntimeException{
    public static final String PUBLISHER_NOT_FOUND = "Publisher  \'%s\'  Not Found System !!!";
    private static final long serialVersionUID = 1L;

    public PublisherNotFoundException(String email) {
        super(String.format(PUBLISHER_NOT_FOUND, email));
    }
}
