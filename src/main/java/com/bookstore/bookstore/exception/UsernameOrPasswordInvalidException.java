package com.bookstore.bookstore.exception;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;

public class UsernameOrPasswordInvalidException  extends RuntimeException {

    public static final String USERNAME_OR_PASSWORD_INVALID = "username and password are wrong";
    public UsernameOrPasswordInvalidException(String msg) {
        super(USERNAME_OR_PASSWORD_INVALID);
    }
}
