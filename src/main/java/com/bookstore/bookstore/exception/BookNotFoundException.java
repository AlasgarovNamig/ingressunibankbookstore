package com.bookstore.bookstore.exception;

public class BookNotFoundException extends RuntimeException {
    public static final String BOOK_NOT_FOUND = "Book \'%s\'  Not Found System !!!";
    private static final long serialVersionUID = 1L;

    public BookNotFoundException(String book) {
        super(String.format(BOOK_NOT_FOUND, book));
    }
}
