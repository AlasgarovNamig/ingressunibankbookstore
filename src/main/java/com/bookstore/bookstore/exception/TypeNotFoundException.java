package com.bookstore.bookstore.exception;


public class TypeNotFoundException extends RuntimeException {
    public static final String TYPE_NOT_FOUND = "User type  \'%s\'  dont supported!!!";
    private static final long serialVersionUID = 1L;

    public TypeNotFoundException(String type) {
        super(String.format(TYPE_NOT_FOUND, type));
    }
}
