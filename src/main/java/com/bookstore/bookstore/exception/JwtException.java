package com.bookstore.bookstore.exception;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;

public class JwtException  extends ExpiredJwtException {
    public static final String PUBLISHER_NOT_FOUND = "Token expired";
    public JwtException(Header header, Claims claims, String message) {
        super(header, claims, PUBLISHER_NOT_FOUND);
    }
}
