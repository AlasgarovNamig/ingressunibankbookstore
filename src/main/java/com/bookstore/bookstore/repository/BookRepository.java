package com.bookstore.bookstore.repository;

import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    Page<Book> findByNameLike(String name, Pageable pageable);

    @Query("SELECT book from Book as book where book.publisher_id.id = :publisher_id")
    List<Book> findByPublisher_id(Long publisher_id);

}
