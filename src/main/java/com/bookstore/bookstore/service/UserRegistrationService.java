package com.bookstore.bookstore.service;


import com.bookstore.bookstore.dto.request.UserInfoDto;
import freemarker.template.TemplateException;

import javax.mail.MessagingException;
import java.io.IOException;

public interface UserRegistrationService {
    void sendActivationEmail(UserInfoDto dto) throws IOException, TemplateException, MessagingException;
}
