package com.bookstore.bookstore.service.impl;


import com.bookstore.bookstore.dto.request.SignUpDto;
import com.bookstore.bookstore.dto.request.TokenPayloadDto;
import com.bookstore.bookstore.dto.request.UserInfoDto;
import com.bookstore.bookstore.dto.request.VerificationTokenDto;
import com.bookstore.bookstore.exception.EmailAlreadyUsedException;
import com.bookstore.bookstore.model.User;
import com.bookstore.bookstore.model.enums.UserType;
import com.bookstore.bookstore.repository.AuthorityRepository;
import com.bookstore.bookstore.repository.UserRepository;
import com.bookstore.bookstore.security.SecurityProperties;
import com.bookstore.bookstore.security.service.JwtService;
import com.bookstore.bookstore.service.UserRegistrationService;
import com.bookstore.bookstore.service.UserService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import liquibase.pro.packaged.V;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {
    private static final Duration FIVE_MINUTE = Duration.ofMinutes(5);
    private static final String ACTIVATION_KEY = "QWxtYSAgQXJtdWQgSGV5dmEgTmFyIHZlIGJ1dHVuIG1leXZlIGFsZW1pbmkgeWlnZGltIGJ1cmEgYW1hIGJ1IGJ5dGUgbWVzZWxlc2kgY2FuYSB5aWdkaSB1amUgLg==";
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final UserRegistrationService userRegistrationService;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.trace("Authenticating: {}", email);
        return userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException(
                                String.format("User %s was not found in the database", email)));
    }

    @Override
    public void signUp(SignUpDto signUpDto) {
        userRepository.findByEmailAndAccountNonExpiredAndAccountNonLockedAndCredentialsNonExpiredAndEnabled(
                signUpDto.getEmail(),
                true,
                true,
                true,
                true).ifPresent(user -> {
            throw new EmailAlreadyUsedException(signUpDto.getEmail());
        });
            User user =modelMapper.map(signUpDto,User.class);

        if (signUpDto.getUserType().equals(UserType.USER)) {
            user.setAuthorities(Set.of(authorityRepository.findByName(UserType.USER.toString()).get()));
        } else if (signUpDto.getUserType().equals(UserType.PUBLISHER)) {
            user.setAuthorities(Set.of(authorityRepository.findByName(UserType.PUBLISHER.toString()).get()));
        }

        userRepository.save(user);
        sendActivateEmail(signUpDto);
    }

    @Override
    public ResponseEntity<Void> activate(String token) {
        try {
            TokenPayloadDto tokenPayloadDto = getPayloadToken(token);
            User user = userRepository.findByEmail(tokenPayloadDto.getSub()).get();
            user.setAccountNonExpired(true);
            user.setAccountNonLocked(true);
            user.setCredentialsNonExpired(true);
            user.setEnabled(true);
            userRepository.save(user);
            return ResponseEntity.ok().build();
        }catch (Exception ex){
            log.trace(ex.getMessage());
            return ResponseEntity.badRequest().build();
        }

    }
    private void sendActivateEmail(SignUpDto signUpDto){
                VerificationTokenDto accessTokenDto = VerificationTokenDto.builder()
                .token(issueTokenForActivate(signUpDto, FIVE_MINUTE))
                .build();
        UserInfoDto userInfoDto = UserInfoDto.builder()
                .email(signUpDto.getEmail())
                .verificationToken(accessTokenDto)
                .build();
        try {
            userRegistrationService.sendActivationEmail(userInfoDto);
        } catch (Exception ex) {
            log.error("User {} Activate Process Crashed !!!", signUpDto.getEmail());
        }
    }

    private String issueTokenForActivate(SignUpDto signUpDto,Duration duration) {
        byte[] keyBytes = Decoders.BASE64.decode(ACTIVATION_KEY);
        JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(signUpDto.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .setHeader(Map.of("type","JWT"))
                .signWith(Keys.hmacShaKeyFor(keyBytes), SignatureAlgorithm.HS512);
        return jwtBuilder.compact();

    }

    private TokenPayloadDto getPayloadToken(String token) {
        Claims claims = parseTokenForActivateaToken(token);

      try {
        return TokenPayloadDto.builder()
                .sub(claims.get("sub").toString())
                .exp(claims.getExpiration())
                .build();
        }catch (Exception ex){
          log.trace(ex.getMessage());
            return  TokenPayloadDto.builder().build();
        }
    }

    private Claims parseTokenForActivateaToken(String token)  { //throws ExpiredJwtException
        byte[] keyBytes = Decoders.BASE64.decode(ACTIVATION_KEY);
        try {
            return Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(keyBytes))
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
        }catch (Exception ex){
            log.trace(ex.getMessage());
            return  new Claims() {
                @Override
                public String getIssuer() {
                    return null;
                }

                @Override
                public Claims setIssuer(String iss) {
                    return null;
                }

                @Override
                public String getSubject() {
                    return null;
                }

                @Override
                public Claims setSubject(String sub) {
                    return null;
                }

                @Override
                public String getAudience() {
                    return null;
                }

                @Override
                public Claims setAudience(String aud) {
                    return null;
                }

                @Override
                public Date getExpiration() {
                    return null;
                }

                @Override
                public Claims setExpiration(Date exp) {
                    return null;
                }

                @Override
                public Date getNotBefore() {
                    return null;
                }

                @Override
                public Claims setNotBefore(Date nbf) {
                    return null;
                }

                @Override
                public Date getIssuedAt() {
                    return null;
                }

                @Override
                public Claims setIssuedAt(Date iat) {
                    return null;
                }

                @Override
                public String getId() {
                    return null;
                }

                @Override
                public Claims setId(String jti) {
                    return null;
                }

                @Override
                public <T> T get(String claimName, Class<T> requiredType) {
                    return null;
                }

                @Override
                public int size() {
                    return 0;
                }

                @Override
                public boolean isEmpty() {
                    return false;
                }

                @Override
                public boolean containsKey(Object key) {
                    return false;
                }

                @Override
                public boolean containsValue(Object value) {
                    return false;
                }

                @Override
                public Object get(Object key) {
                    return null;
                }

                @Override
                public Object put(String key, Object value) {
                    return null;
                }

                @Override
                public Object remove(Object key) {
                    return null;
                }

                @Override
                public void putAll(Map<? extends String, ?> m) {

                }

                @Override
                public void clear() {

                }

                @Override
                public Set<String> keySet() {
                    return null;
                }

                @Override
                public Collection<Object> values() {
                    return null;
                }

                @Override
                public Set<Entry<String, Object>> entrySet() {
                    return null;
                }
            };
        }

    }
}
