package com.bookstore.bookstore.service.impl;


import com.bookstore.bookstore.dto.request.UserInfoDto;
import com.bookstore.bookstore.service.SendMailService;
import com.bookstore.bookstore.service.UserRegistrationService;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserRegistrationServiceImpl implements UserRegistrationService {

    private static final String SUBJECT = "Book Store  - Account Activation";
    private static final String RESET_PASSWORD_TEXT =
            "Please click link for account activate ";

    private final FreeMarkerConfigurer freemarkerConfigurer;
    private final SendMailService sendMailService;

    @Value("${spring.mail.username}")
    private String hostMail;
    @Value("${url.bookstore}")
    private String bookstoreUrl;

    @Override
    public void sendActivationEmail(UserInfoDto dto) throws IOException, TemplateException, MessagingException {
        System.out.println(dto);
        log.trace("Sending activation email to {}", dto.getEmail());
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("text", RESET_PASSWORD_TEXT);
        templateModel.put("senderName", hostMail);
        templateModel.put("activationUrl",
                String.format("http://%s/auth/activate?token=%s", bookstoreUrl,
                        dto.getVerificationToken().getToken(), dto.getEmail()));
        Template template = freemarkerConfigurer.getConfiguration().getTemplate("registration-activation-template.ftl");
        String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateModel);
        sendMailService.send(dto.getEmail(), SUBJECT, htmlBody);
        log.info("Email for account activation sent to email {}", dto.getEmail());
    }
}
