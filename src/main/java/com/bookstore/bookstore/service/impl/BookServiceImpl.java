package com.bookstore.bookstore.service.impl;

import com.bookstore.bookstore.dto.request.BookDto;
import com.bookstore.bookstore.dto.response.BookResponseDto;
import com.bookstore.bookstore.exception.BookNotFoundException;
import com.bookstore.bookstore.exception.PublisherNotFoundException;
import com.bookstore.bookstore.model.Book;
import com.bookstore.bookstore.model.User;
import com.bookstore.bookstore.repository.BookRepository;
import com.bookstore.bookstore.repository.UserRepository;
import com.bookstore.bookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<BookResponseDto> getAll() {
        return bookRepository.findAll().stream()
                .map(book -> modelMapper.map(book, BookResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Page<BookResponseDto> searchName(String searchName, Pageable page) {
        return bookRepository.findByNameLike("%"+searchName+"%", page)
                .map(book -> modelMapper.map(book, BookResponseDto.class));
    }

    @Override
    @Transactional
    public BookResponseDto bookDetail(Long id) {
        return bookRepository.findById(id)
                .map(book -> modelMapper.map(book, BookResponseDto.class))
                .orElseThrow(()-> new BookNotFoundException(id.toString()));
    }

    @Override
    public List<BookResponseDto> publisherBooks(Long publisherId) {
        return  bookRepository.findByPublisher_id(publisherId).stream()
                .map(book -> modelMapper.map(book, BookResponseDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void bookCreate(BookDto bookCreateDto, Authentication authentication) {
        User user = userRepository.findByEmail(authentication.getName()).orElseThrow(() -> new PublisherNotFoundException(authentication.getName()));
        Book book = Book.builder()
                .name(bookCreateDto.getName())
                .author(bookCreateDto.getAuthor())
                .title(bookCreateDto.getTitle())
                .coast(bookCreateDto.getCoast())
                .publisher_id(user)
                .build();
        bookRepository.save(book);
    }

    @Override
    public ResponseEntity<Void> bookUpdate(BookDto bookDto, Authentication authentication, Long id) {
        User user = userRepository.findByEmail(authentication.getName()).orElseThrow(() -> new PublisherNotFoundException(authentication.getName()));
        Book book = bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id.toString()));
        System.out.println("book"+book.getPublisher_id().getId());
        System.out.println("user"+user.getId());
        if (book.getPublisher_id().getId() == user.getId()) {
            book.setName(bookDto.getName());
            book.setAuthor(bookDto.getAuthor());
            book.setTitle(bookDto.getTitle());
            book.setCoast(bookDto.getCoast());
            bookRepository.save(book);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
}
