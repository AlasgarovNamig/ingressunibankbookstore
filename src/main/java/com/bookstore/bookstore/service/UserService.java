package com.bookstore.bookstore.service;

import com.bookstore.bookstore.dto.request.SignUpDto;
import org.springframework.http.ResponseEntity;

public interface UserService {
    void signUp(SignUpDto signUpDto);
    ResponseEntity<Void> activate(String email);
}
