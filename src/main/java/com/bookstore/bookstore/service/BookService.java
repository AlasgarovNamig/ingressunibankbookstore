package com.bookstore.bookstore.service;

import com.bookstore.bookstore.dto.request.BookDto;
import com.bookstore.bookstore.dto.response.BookResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface BookService {
    List<BookResponseDto> getAll();
    Page<BookResponseDto> searchName(String searchName, Pageable pageable);
    BookResponseDto bookDetail(Long id);
    List<BookResponseDto> publisherBooks(Long publisherId);
    void bookCreate(BookDto bookCreateDto, Authentication authentication);
    ResponseEntity<Void> bookUpdate(BookDto bookCreateDto, Authentication authentication, Long id);
}
