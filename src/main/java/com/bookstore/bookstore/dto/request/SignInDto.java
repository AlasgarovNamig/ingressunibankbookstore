package com.bookstore.bookstore.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SignInDto {
    private String email;
    private String password;
    private Boolean rememberMe;
}
