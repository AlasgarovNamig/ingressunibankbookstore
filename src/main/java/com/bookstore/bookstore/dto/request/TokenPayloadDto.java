package com.bookstore.bookstore.dto.request;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class TokenPayloadDto {
    private String sub;
    private Date exp;
}
