package com.bookstore.bookstore.dto.request;

import com.bookstore.bookstore.model.enums.UserType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SignUpDto {
    private String fullName;
    private String email;
    private String password;
    private UserType userType;
}
