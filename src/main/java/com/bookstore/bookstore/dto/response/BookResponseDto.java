package com.bookstore.bookstore.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookResponseDto {
    private String name;
    private String title;
    private String author;
    private BigDecimal coast;
    private UserResponseDto publisher_id;
}
